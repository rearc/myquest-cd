# CI/CD Pipeline for Amazon ECS/Fargate
Creation of CI/CD pipeline for Amazon ECS/Fargate

# Architecture

The following AWS Servies were used and setup using terraform.
The terraform script is main.tf

1. ECR
2. CodeCommit
3. CodeBuild
4. CodePipeline
5. VPC
6. Load Balancer
7. ECS Fargate Cluster
8. Tasks
9. Service

# Setup of Infratructure

## To create the infrastructure.

First we need to synthesize an execution plan for terraform using following command:

```shell
terraform plan -out main.out
```

Once we have the plan we can apply this.

```shell
terraform apply "main.out" -auto-approve
```

## To cleanup and delete infrastructure.

```shell
terraform destroy
```

## After applying the terraform script and  pushing the application code
The pipeline kicked in and pushed the dcoker image to ECR which then allowed the tasks to be applied.

The following was oberved in the browser going to following [lb url](http://app-lb-1402188595.us-east-1.elb.amazonaws.com/):

![image info](./docs/pics/myquest-rearc.png)

The secret was revealed the terraform script updated and redeployed, this caused the

# What If I had more time?

## Create code commit, build for automatically building and deploying the terraform and maintain the infrastructure!

## Repeat this using [projen](https://github.com/projen/projen) and create a version using AWS CDK as a construct.

So the initial command after globally installing projen would be:

```shell
projen create awscdk-construct cdk-quest
```

For this Contruct I would externalize all the variables and be able to detect if the user specifies their own CodeCommit, CodeDeploy, ECR or VPC to instantiate this "Quest" construct.
If not create the complete infrastructure for the loadbalance, fargate cluster, tasks and service.

## Create tests for the cdk-quest contruct.

## Other observations and Notes
Not exactly sure why the response said that "You dont seem to be running in a Docker container."
It is clear that "Hmmmm..... you have not configured TLS (https) yet OR your request did not traverse via an https endpoint" is observed. If i invested more time this would be one of the first implementation to do. I could use cloudfront and route53 to setup a domain and foward the request from here to the load balancer. Using Authentication and authorization would be an aditional enhacement providing security to the app.

It did take me a quite a bit of time to complete this about 12hrs. Most of my time was spend on terraform documentation and to come up with the right constructs especially setting up the VPC and network.