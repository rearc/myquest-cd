#!/bin/bash

#Required
domain=$1
commonname=${domain}

#Change to your company details
country=US
state=Texas
locality=Austin
organization=drgeb.com
organizationalunit=IT
email=administrator@drgeb.com

#Optional
password=dummypassword

if [ -z "${domain}" ]
then
    echo "Argument not present."
    echo "Useage $0 [common name]"

    exit 99
fi

echo "Generating key request for ${domain}"

#Generate a key
openssl genrsa  -out ${domain}.key 4096

#Remove passphrase from the key. Comment the line out to keep the passphrase
echo "Removing passphrase from key"
openssl rsa -in ${domain}.key -passin pass:${password} -out ${domain}.key

#Create the request
echo "Creating CSR"
openssl req -new -x509 -nodes -sha1 -days 3650 -extensions v3_ca -key ${domain}.key -out ${domain}.csr -passin pass:${password} \
    -subj "/C=${country}/ST=${state}/L=${locality}/O=${organization}/OU=${organizationalunit}/CN=${commonname}/emailAddress=${email}"

echo "---------------------------"
echo "-----Below is your CSR-----"
echo "---------------------------"
echo
# cat ${domain}.csr

echo
echo "---------------------------"
echo "-----Below is your Key-----"
echo "---------------------------"
echo
# cat ${domain}.key
