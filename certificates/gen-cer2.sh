#!/bin/bash

#Required
domain=$1
commonname=${domain}

#Change to your company details
country=US
state=Texas
locality=Austin
organization=drgeb.com
organizationalunit=IT
email=administrator@drgeb.com

#Optional
password=dummypassword

if [ -z "${domain}" ]
then
    echo "Argument not present."
    echo "Useage $0 [common name]"

    exit 99
fi

echo "Generating key request for ${domain}"

# 1. Generate self-sign certificate using this command:
openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -keyout privateKey.key -out certificate.crt -subj "/C=${country}/ST=${state}/L=${locality}/O=${organization}/OU=${organizationalunit}/CN=${commonname}/emailAddress=${email}"

# Verify the key and certificate generated
openssl rsa -in privateKey.key -check
openssl x509 -in certificate.crt -text -noout

# 3. Convert the key and cert into .pem encoded file
openssl rsa -in privateKey.key -text > private.pem
openssl x509 -inform PEM -in certificate.crt > public.pem